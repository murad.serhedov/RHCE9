<?php
require_once "config.php";

// Veritabanından veri seçin
$sorgu = "SELECT * FROM Book";
if($sonuc = mysqli_query($baglanti, $sorgu)){
    if(mysqli_num_rows($sonuc) > 0){
        echo "<table>";
            echo "<tr>";
                echo "<th>ID</th>";
                echo "<th>İsim</th>";
                echo "<th>Email</th>";
            echo "</tr>";
        while($row = mysqli_fetch_array($sonuc)){
            echo "<tr>";
                echo "<td>" . $row['id'] . "</td>";
                echo "<td>" . $row['isim'] . "</td>";
                echo "<td>" . $row['email'] . "</td>";
            echo "</tr>";
        }
        echo "</table>";
        // Sonuç setini serbest bırak
        mysqli_free_result($sonuc);
    } else{
        echo "Veri bulunamadı.";
    }
} else{
    echo "Hata: $sorgu. " . mysqli_error($baglanti);
}
// Bağlantıyı kapat
mysqli_close($baglanti);
?>

