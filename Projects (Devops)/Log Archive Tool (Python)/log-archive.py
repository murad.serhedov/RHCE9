#!/usr/bin/env python3
import os
import tarfile
import time
import sys
from datetime import datetime

# Log dizinini ve arşivleme dizinini argüman olarak alıyoruz
def archive_logs(log_dir):
    # Eğer log dizini mevcut değilse hata mesajı veriyoruz
    if not os.path.exists(log_dir):
        print(f"Error: The directory {log_dir} does not exist.")
        sys.exit(1)

    # Arşivleri saklayacağımız dizin
    archive_dir = os.path.join(log_dir, "archived_logs")
    os.makedirs(archive_dir, exist_ok=True)

    # Tarih ve saat formatı
    current_time = datetime.now().strftime('%Y%m%d_%H%M%S')
    archive_name = f"logs_archive_{current_time}.tar.gz"
    archive_path = os.path.join(archive_dir, archive_name)

    # Log dosyalarını tar.gz formatında sıkıştırıyoruz
    with tarfile.open(archive_path, "w:gz") as tar:
        tar.add(log_dir, arcname=os.path.basename(log_dir))
    
    # Arşivleme zamanını log dosyasına yazıyoruz
    log_entry = f"Logs archived at {current_time}, stored at {archive_path}\n"
    with open(os.path.join(archive_dir, "archive_log.txt"), "a") as log_file:
        log_file.write(log_entry)
    
    print(f"Logs have been successfully archived at {archive_path}")

# Kullanıcıdan log dizinini alıyoruz
if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: log-archive <log-directory>")
        sys.exit(1)
    
    log_directory = sys.argv[1]
    archive_logs(log_directory)

