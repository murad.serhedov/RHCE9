#!/bin/bash

# Get total CPU usage
echo "===== CPU Usage ====="
cpu_usage=$(top -bn1 | grep "Cpu(s)" | awk '{print 100 - $8 "%"}')
echo "Total CPU Usage: $cpu_usage"

# Get total memory usage
echo "===== Memory Usage ====="
mem_info=$(free -m)
total_mem=$(echo "$mem_info" | awk 'NR==2{print $2}')
used_mem=$(echo "$mem_info" | awk 'NR==2{print $3}')
free_mem=$(echo "$mem_info" | awk 'NR==2{print $4}')
mem_percentage=$(echo "$used_mem $total_mem" | awk '{print ($1/$2) * 100}')
echo "Total Memory: $total_mem MB"
echo "Used Memory: $used_mem MB"
echo "Free Memory: $free_mem MB"
printf "Memory Usage: %.2f%%\n" "$mem_percentage"

# Get total disk usage
echo "===== Disk Usage ====="
disk_info=$(df -h /)
total_disk=$(echo "$disk_info" | awk 'NR==2{print $2}')
used_disk=$(echo "$disk_info" | awk 'NR==2{print $3}')
free_disk=$(echo "$disk_info" | awk 'NR==2{print $4}')
disk_percentage=$(echo "$disk_info" | awk 'NR==2{print $5}')
echo "Total Disk: $total_disk"
echo "Used Disk: $used_disk"
echo "Free Disk: $free_disk"
echo "Disk Usage: $disk_percentage"

# Top 5 processes by CPU usage
echo "===== Top 5 Processes by CPU Usage ====="
ps -eo pid,ppid,cmd,%cpu --sort=-%cpu | head -6

# Top 5 processes by memory usage
echo "===== Top 5 Processes by Memory Usage ====="
ps -eo pid,ppid,cmd,%mem --sort=-%mem | head -6

